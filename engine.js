/*
This code was addapted from the source provided by Eric House and Fixed Innovations.
http://eehouse.org/fixin/source.php
*/


function singleSpeedEngine(){
    // Private vars
    var that = {};
    var nodes = [];
    var isMetric = false;
    
    // Private functions
    var buildNodeList = function(bigRing, smallRing, bigCog, smallCog, stayLen, wheelDiam, stretch, useHalfLink, isMetric)
    {
        this.isMetric = isMetric === 'false' ? false : true;
        nodes = this.nodes = [];
        if (this.isMetric){stayLen = stayLen / 2.54};
        useHalfLink = useHalfLink === 'false' ? false : true;
        var _minStayLen = stayLen - (STAYRANGE/2.0);
        var _maxStayLen = stayLen + (STAYRANGE/2.0);
    
        for (_ring = bigRing; _ring >= smallRing; _ring-- ) {
            for (_cog = bigCog; _cog >= smallCog; _cog-- ) {
                var _perim = perimeterFromGear(_ring, _cog, _minStayLen);
                var _minHalfLinks = chainBound(_perim, stretch, true, useHalfLink );
    
                _perim = perimeterFromGear(_ring, _cog, _maxStayLen);
                var _maxHalfLinks = chainBound(_perim, stretch, false, useHalfLink );
    
                for (_curHalfLinks = _minHalfLinks; _curHalfLinks <= _maxHalfLinks; _curHalfLinks += useHalfLink ? 1 : 2 ) {
                    // start too big and work down
                    var _curStayLength = calcStayForGear(_ring, _cog, stretched(_curHalfLinks * 0.5, stretch ), _maxHalfLinks);
          
                    if ( (_curStayLength <= _maxStayLen ) && (_curStayLength >= _minStayLen) ) {
                        // now print the chainlength.  If it's for display to the
                        // user, we need it in inches (rather than half-inches),
                        // but unless halfLinks are enabled there's no need to
                        // print as a float since there won't be any half sizes.
                        
                        nodes.push( [_ring, _cog, _curStayLength, _curHalfLinks/2, (_ring*wheelDiam)/_cog] );
                    }
                }
            }
        }
    
        return nodes;
    }
    
    var perimeterFromGear = function(ring, cog, stayLength)
    {
        var _cogRadius = cog / (4 * Math.PI);
        var _ringRadius = ring / (4 * Math.PI);
    
        var _theta = Math.acos((_ringRadius - _cogRadius)/stayLength) * (180/Math.PI);
    
        var _result = ((180 - _theta) * Math.PI * _ringRadius + _theta * Math.PI * _cogRadius + 180 * Math.sqrt(Math.pow(stayLength, 2) - Math.pow((_ringRadius - _cogRadius),2))) / 90;    
    
        return _result;
    }
    
    var chainBound = function(d, stretch, upper, useHalfLink)
    {
        // considering stretch and whether halfLinks are enabled, return
        // the number of halflinks needed to create the chain whose length
        // is just above or below d
        
        var _oneLink = stretched(useHalfLink? 0.5: 1.0, stretch);
        var _linkCount = Math.floor(d / _oneLink);
        var _temp = _oneLink * _linkCount;
      
        if ( (upper) && (_temp != d) ) {
              _linkCount++;
        }
      
        if ( !useHalfLink ) {
              _linkCount *= 2;
        }
      
        return _linkCount;
    }
    
    var stretched = function(chainlen, stretch)
    {
        return chainlen + ((chainlen * stretch) / 12 );
    }
    
    var calcStayForGear = function (ring, cog, chainLen, trialStayLength )
    {
      var _kTolerance = 0.0001;
      var _counter = 6;
    
      while ( _counter > 0 ) {
        // compare with chainLen (target) and adjust
        var _curDiff = perimeterFromGear(ring, cog, trialStayLength ) - chainLen;
    
        // this is ok as the only way it can be below 0 is if greater than -kTolerance
    
        if ( _curDiff < _kTolerance ) {
            break;
        }
        else {
            trialStayLength -= _curDiff/2.0;
        }
        _counter--;
      }
    
      if ( _counter <= 0 ) {
            trialStayLength = 0.0;
      }
    
      return trialStayLength;
    }
    
    // Expose Public methods/data
    
    that.buildNodeList = buildNodeList;
    that.nodes = nodes;
    that.isMetric = isMetric;
    
    return that;
}

